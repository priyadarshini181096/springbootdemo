package com.example.main.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	 @GetMapping(value = "/")
	    public @ResponseBody String getCities() {
	        return "ok";
	    }

}
